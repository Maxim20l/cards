const modalWindow = document.querySelector('.positionWindowvisit');
const closeModal = document.querySelector('.closeModal');
const doctor = document.querySelector('#doctors');
const doctors = document.querySelectorAll('.form-doctor');
const registBtn = document.querySelector('.registerbtn');
const form = document.querySelector('.form-window');
const visitcards = document.querySelector('.visit-cards');
const closeCard = document.querySelector('.closeCard');
const radioStatusHigh = document.getElementById('statusHigh');
const radioStatusNormal = document.getElementById('statusNormal');
const radioStatusLow = document.getElementById('statusLow');
const checkBoxStatusHigh = document.getElementById('checkStatusHigh');
const checkBoxStatusNormal = document.getElementById('checkStatusNormal');
const checkBoxStatusLow = document.getElementById('checkStatusLow');
const filterByPurpose = document.querySelector('.filterByPurpose');
let btnAuthorizationModal = document.querySelector('.authorization');
const closeAuthorizWindow = document.querySelector('.closeAuthorizWindow');
const registration = document.querySelector('.registration');
const btnAuthorization = document.querySelector('.btnAuthoriz');
const authorizationform = document.querySelector('.authorizationform');
let tokenLocal;


function showDetailsCard(event) {
    let closedElem = event.target.parentElement.querySelectorAll('.closed');
    for (let apear of closedElem) {
        apear.classList.remove('closed');
    }
    event.target.remove();
}


async function getCards() {
    try {
        let response = await fetch('https://ajax.test-danit.com/api/v2/cards', {
            headers: {
                'Authorization': `Bearer ${tokenLocal}`
            }
        })
        if (response.ok) {
            let result = await response.json();
            result.forEach((element) => {
                let { doctorName } = element;
                if (doctorName === 'Cardiologist') {
                    let visitCardiologist = new VisitCardiologist(element);
                    visitCardiologist.render(visitcards)
                } else if (doctorName === 'Dentist') {
                    let visitDentist = new VisitDentist(element);
                    visitDentist.render(visitcards);
                } else if (doctorName === 'Therapist') {
                    let visitTherapist = new VisitTherapist(element);
                    visitTherapist.render(visitcards);
                }
            })
        } else {
            throw new Error('We have some troubles with serve');
        }

    } catch (error) {
        visitcards.innerHTML = `<p>${error.message}</p>`
    }
}


closeAuthorizWindow.addEventListener('click', () => {
    registration.style.display = 'none';
    if (document.querySelector('.error')) {
        document.querySelector('.error').remove()

    }
})

btnAuthorizationModal.addEventListener('click', () => {
    if (!btnAuthorizationModal.classList.contains('openModalWindow')) {
        registration.style.display = 'block';
    } else {
        modalWindow.style.display = 'block';
    }


})

class Modal {
    constructor(password, mail) {
        this.password = password;
        this.mail = mail;
    }
    async checkAccount() {
        try {
            let response = await fetch("https://ajax.test-danit.com/api/v2/cards/login", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ email: `${this.mail}`, password: `${this.password}` })
            })
            if (response.status >= 200 && response.status < 300) {
                let token = await response.text();
                localStorage.removeItem('token');
                localStorage.setItem('token', token);
                tokenLocal = localStorage.getItem('token');
                registration.style.display = 'none';
                btnAuthorizationModal.classList.add('openModalWindow');
                btnAuthorizationModal.innerText = 'Створити візит';
                await getCards();
            } else {
                throw new Error('Not correct Password of Login');
            }
        } catch (error) {
            if (!document.querySelector('.error')) {
                authorizationform.insertAdjacentHTML('beforeend', `
                <p class="error">${error}</p>
                `)
            }


        }

    }
}


btnAuthorization.addEventListener('click', (event) => {
    const password = document.querySelector('.passwordAuthoriz').value;
    const mail = document.querySelector('.mailAuthoriz').value;
    let modalAuthoriz = new Modal(password, mail);
    modalAuthoriz.checkAccount()
    const valueInputs = document.querySelectorAll('input');
    valueInputs.forEach(element => element.value = '');
    event.preventDefault()

})




filterByPurpose.addEventListener('keyup', filterCardsByText);

function filterCardsByText() {
    let purposeCard = document.querySelectorAll('.purposeCard');
    let descriptionCard = document.querySelectorAll('.descriptionCard');
    let arrCards = [];

    for (let i = 0; i < purposeCard.length; i++) {
        if (purposeCard[i].dataset.purpose.indexOf(this.value) < 0) {
            if (purposeCard[i].parentElement.classList.contains('active')) {
                if (descriptionCard[i].dataset.description.indexOf(this.value) < 0) {
                    if (descriptionCard[i].parentElement.classList.contains('active')) {
                        arrCards.push(descriptionCard[i].parentElement);
                        continue;
                    }
                }

            }
        }

    }
    for (let aim of purposeCard) {
        if (aim.parentElement.classList.contains('active')) {
            aim.parentElement.style.display = 'flex';
            if (!aim.parentElement.classList.contains('filterByText')) {
                aim.parentElement.classList.add('filterByText');
            }
        }
    }
    if (arrCards.length === 0) {
        for (const aim of purposeCard) {
            aim.parentElement.classList.add('filterByText');
        }
    }
    for (let card of arrCards) {
        card.style.display = 'none';
        card.classList.remove('filterByText');
    }
}







const arrCheckBoxes = [checkBoxStatusLow, checkBoxStatusNormal, checkBoxStatusHigh];

checkBoxStatusNormal.addEventListener('change', checkCardByStatus);
checkBoxStatusHigh.addEventListener('change', checkCardByStatus);
checkBoxStatusLow.addEventListener('change', checkCardByStatus);


function checkCardByStatus() {
    let descriptionStatusCards = document.querySelectorAll('.descriptionStatus');
    let arrChecked = [];
    let count = 0;
    for (let box of arrCheckBoxes) {


        if (box.checked) {
            descriptionStatusCards.forEach((element) => {


                if (element.dataset.status == box.name) {

                    if (element.parentElement.classList.contains('filterByText')) {
                        arrChecked.push(element.parentElement);
                    }

                }


            })
        }
    }

    for (let cards of descriptionStatusCards) {
        cards.parentElement.style.display = 'none';
        cards.parentElement.classList.remove('active');
    }


    for (let cards of arrChecked) {
        cards.style.display = 'flex';
        cards.classList.add('active');
    }
    for (let box of arrCheckBoxes) {
        if (!box.checked) {
            count += 1;
        }
        if (count === arrCheckBoxes.length) {
            for (let cards of descriptionStatusCards) {
                cards.parentElement.classList.add('active');
                if (cards.parentElement.classList.contains('filterByText')) {
                    cards.parentElement.style.display = 'flex';
                }
            }
        }
    }
}



let deleteCard = (id) => {
    const card = document.querySelector(`.card_${id}`);
    fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${tokenLocal}`
        },
    })
        .then(response => {
            if (response.ok) {
                card.remove();

            }
        })
}

class VisitDentist {
    constructor({ doctorName, purpose, description, status, namePacient, lastVisit, id }) {
        this.doctorName = doctorName;
        this.purpose = purpose;
        this.description = description;
        this.status = status;
        this.namePacient = namePacient;
        this.lastVisit = lastVisit;
        this.id = id;
    }
    render(container) {
        container.insertAdjacentHTML('afterbegin', `
        <div id='${this.id}' class='card card_${this.id} active filterByText'>
        <span class="closeCard" onclick = 'deleteCard(${this.id})'></span>
        <p class="nameDoctorCard">${this.doctorName}</p>
        <p class='purposeCard' data-purpose=${this.purpose}>${this.purpose}</p>
        <p class='descriptionCard' data-description=${this.description}>${this.description}</p>
        <p class="descriptionStatus" data-status=${this.status}>${this.status}</p>
        <p class='namePacientCard'>${this.namePacient}</p>
        <p class='lastVisitCard closed'>${this.lastVisit}</p>
        <button class="show" onclick="showDetailsCard(event)">Show more</button>
    </div>
        `);
    }
}
class VisitCardiologist {
    constructor({ doctorName, purpose, description, status, namePacient, pressure, bodyIndex, diseases, cardioAge, id }) {
        this.doctorName = doctorName;
        this.purpose = purpose;
        this.description = description;
        this.status = status;
        this.namePacient = namePacient;
        this.pressure = pressure;
        this.bodyIndex = bodyIndex;
        this.diseases = diseases;
        this.cardioAge = cardioAge;
        this.id = id;
    }
    render(container) {
        container.insertAdjacentHTML('afterbegin', `
        <div id='${this.id}' class='card card_${this.id} active filterByText'>
        <span class="closeCard" onclick = 'deleteCard(${this.id})'></span>
        <p class="nameDoctorCard">${this.doctorName}</p>
        <p class='purposeCard' data-purpose=${this.purpose}>${this.purpose}</p>
        <p class='descriptionCard' data-description=${this.description}>${this.description}</p>
        <p class="descriptionStatus" data-status=${this.status}>${this.status}</p>
        <p class='namePacientCard'>${this.namePacient}</p>
        <p class='pressureCard closed'>${this.pressure}</p>
        <p class='bodyIndexCard closed'>${this.bodyIndex}</p>
        <p class='diseasesCard closed'>${this.diseases}</p>
        <p class='ageCard closed'>${this.cardioAge}</p>
        <button class="show" onclick="showDetailsCard(event)">Show more</button>
    </div>
        `);
    }
}

class VisitTherapist {
    constructor({ doctorName, purpose, description, status, namePacient, therapistAge, id }) {
        this.doctorName = doctorName;
        this.purpose = purpose;
        this.description = description;
        this.status = status;
        this.namePacient = namePacient;
        this.therapistAge = therapistAge;
        this.id = id;
    }
    render(container) {
        container.insertAdjacentHTML('afterbegin', `
        <div id='${this.id}' class='card card_${this.id} active filterByText'>
        <span class="closeCard" onclick = 'deleteCard(${this.id})'></span>
        <p class="nameDoctorCard">${this.doctorName}</p>
        <p class='purposeCard' data-purpose=${this.purpose}>${this.purpose}</p>
        <p class='descriptionCard' data-description=${this.description}>${this.description}</p>
        <p class="descriptionStatus" data-status=${this.status}>${this.status}</p>
        <p class='namePacientCard'>${this.namePacient}</p>
        <p class='ageCard closed'>${this.therapistAge}</p>
        <button class="show" onclick="showDetailsCard(event)">Show more</button>
    </div>
        `);
    }
}

form.addEventListener('submit', (event) => {
    const doctorName = doctor.value;
    const purpose = document.querySelector('.purpose').value;
    const description = document.querySelector('.description').value;
    const statuses = document.getElementsByName('urgency');
    let status;
    statuses.forEach(element => {
        if (element.checked) {
            status = element.value;
        }
    })

    const namePacient = document.querySelector('.namePacient').value;
    if (doctorName == 'Cardiologist') {
        const pressure = document.querySelector('.pressure').value;
        const bodyIndex = document.querySelector('.bodyIndex').value;
        const diseases = document.querySelector('.diseases').value;
        const cardioAge = document.querySelector('.cardioAge').value;
        fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${tokenLocal}`
            },
            body: JSON.stringify({
                doctorName: `${doctorName}`,
                purpose: `${purpose}`,
                description: `${description}`,
                status: `${status}`,
                namePacient: `${namePacient}`,
                pressure: `${pressure}`,
                bodyIndex: `${bodyIndex}`,
                diseases: `${diseases}`,
                cardioAge: `${cardioAge}`,
            })
        })
            .then(response => response.json())
            .then(result => {
                let visitCardiologist = new VisitCardiologist(result);
                visitCardiologist.render(visitcards)

            })



    } else if (doctorName == 'Dentist') {
        const lastVisit = document.querySelector('.lastVisit').value;
        fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${tokenLocal}`
            },
            body: JSON.stringify({
                doctorName: `${doctorName}`,
                purpose: `${purpose}`,
                description: `${description}`,
                status: `${status}`,
                namePacient: `${namePacient}`,
                lastVisit: `${lastVisit}`,
            })
        })
            .then(response => response.json())
            .then(result => {
                let visitDentist = new VisitDentist(result);
                visitDentist.render(visitcards);

            })


    } else if (doctorName == 'Therapist') {
        const therapistAge = document.querySelector('.therapistAge').value;
        fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${tokenLocal}`
            },
            body: JSON.stringify({
                doctorName: `${doctorName}`,
                purpose: `${purpose}`,
                description: `${description}`,
                status: `${status}`,
                namePacient: `${namePacient}`,
                therapistAge: `${therapistAge}`,
            })
        })
            .then(response => response.json())
            .then(result => {
                let visitTherapist = new VisitTherapist(result);
                visitTherapist.render(visitcards);

            })
    }
    const valueInputs = document.querySelectorAll('input');
    valueInputs.forEach(element => element.value = '');
    event.preventDefault()
})

doctor.addEventListener('change', function () {
    const nameOfDoctor = this.value;
    doctors.forEach(element => {
        element.style.display = 'none';
        if (nameOfDoctor === element.dataset.name) {
            element.style.display = 'block';
        }
    })
})

closeModal.addEventListener('click', () => {
    modalWindow.style.display = 'none';
})

registBtn.addEventListener('click', () => {
    modalWindow.style.display = 'none';
})


