const gulp        = require('gulp');
const browserSync = require('browser-sync').create();

function serve(cb) {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    cb();
}
exports.serve = serve;
